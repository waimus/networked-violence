// ChaseCamera.cs
// Attach to camera prefab.
// Basic camera that follow camera with a bit of position lag.
// This camera is not networked. The local player picked this camera
// and then this camera follow the player picking this up.
// Because this is not networked, put one camera on the world

using UnityEngine;

public class ChaseCamera : MonoBehaviour {
    public Transform chaseTarget;
    [SerializeField] private float chaseLag = 2.0f;

    private Vector3 velocity = Vector3.zero;

    // Update is called once per frame
    void Update() {
        ChaseTarget();
    }

    private void ChaseTarget() {
        if (chaseTarget) {
            transform.position = Vector3.Lerp(
                transform.position,
                chaseTarget.position + (Vector3.forward * -10.0f),
                chaseLag * Time.deltaTime
            );
        }
    }
}

// PlayerCamera.cs
// This will look for a camera in scene and make it
// follow the player. This is not to be attached to a camera
// Attach this to a player prefab.

using System.Collections;
using UnityEngine;
using Unity.Netcode;

public class PlayerCamera : NetworkBehaviour {
    private Camera cameraInstance;
    private ChaseCamera chaseCamera;

    private void Start() {
        StartCoroutine(SetCameraTimed(1.0f));
    }

    private void SetCamera() {
        if (IsOwner) {
            cameraInstance = Camera.main;
            chaseCamera = cameraInstance.gameObject.GetComponent<ChaseCamera>();
            chaseCamera.chaseTarget = this.transform;
        }
    }

    private IEnumerator SetCameraTimed(float t) {
        yield return new WaitForSeconds(t);
        SetCamera();
    }
}
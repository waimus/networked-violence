using System.Collections;
using UnityEngine;
using Unity.Netcode;

[RequireComponent(typeof(Rigidbody2D))]
public class ProjectileMovement : NetworkBehaviour {
    [SerializeField] private float launchPower = 8.0f;
    [SerializeField] private float lifetime = 4.0f;
    private Rigidbody2D physicsBody;

    public override void OnNetworkSpawn() {
        if (!IsOwner) {
            this.enabled = false;
        }
    }

    // Start is called before the first frame update
    void Start() {
        physicsBody = GetComponent<Rigidbody2D>();

        // Launch itself
        physicsBody.AddRelativeForce(Vector2.up * launchPower * 10.0f, ForceMode2D.Force);

        // Destroy after running out of lifetime
        StartCoroutine(Lifetime());
    }

    private IEnumerator Lifetime() {
        yield return new WaitForSeconds(lifetime);
        Destroy(this.gameObject);
    }
}

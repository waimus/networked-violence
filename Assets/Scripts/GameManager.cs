// GameManager.cs

using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    // Start is called before the first frame update
    private void Start() {
        ValidateNetworkManager();
    }

    private void ValidateNetworkManager() {
        // See if NetworkManager exists, if not, go to main menu
        if (GameObject.Find("NetworkManager")) {
            return;
        } 

        SceneManager.LoadScene("SCN_MainMenu");
    }
}

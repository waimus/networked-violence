using UnityEngine;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;

public class MenuNetworkSettings : MonoBehaviour {
    [SerializeField] private TMP_InputField inputfieldIPAddress;
    [SerializeField] private TMP_InputField inputfieldPort;
    [SerializeField] private TMP_Text textNetworkStatusInfo;
    private UnityTransport networkTransport;

    private void Start() {
        // Disable this config UI
        gameObject.SetActive(false);

        // Retrieve NetworkManager's NetworkTransport;
        networkTransport = (UnityTransport)NetworkManager.Singleton.NetworkConfig.NetworkTransport;
    }

    // Add this event to inputfield's OnEndEdit
    public void OnConnectionDataChanged() {
        string inputAddress = inputfieldIPAddress.text;
        string inputPort = inputfieldPort.text;

        if (ushort.TryParse(inputPort, out ushort port)) {
            networkTransport.SetConnectionData(inputAddress, port);
        } else {
            // Defaults to port 7777 if parsing port failed
            networkTransport.SetConnectionData(inputAddress, 7777);
        }
    }

    public void UpdateNetworkInfoText() {
        string address = networkTransport.ConnectionData.Address;
        string port = networkTransport.ConnectionData.Port.ToString();
        textNetworkStatusInfo.text = $"Currently connecting to: {address}:{port}. Open settings to configure it.";
    }
}
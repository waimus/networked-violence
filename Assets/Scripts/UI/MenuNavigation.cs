using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Netcode;

public class MenuNavigation : MonoBehaviour {
    [Header("Scenes Configuration")]
    public string sceneGameplay;
    
    public void OnStartHost() {
        if (NetworkManager.Singleton.IsClient && NetworkManager.Singleton.IsServer) {
            return;
        }

        Camera.main.GetComponent<AudioListener>().enabled = false;

        NetworkManager.Singleton.StartHost();
        NetworkManager.Singleton.SceneManager.LoadScene(sceneGameplay, LoadSceneMode.Single);
    }

    public void OnStartClient() {
        if (NetworkManager.Singleton.IsClient && NetworkManager.Singleton.IsServer) {
            return;
        }

        Camera.main.GetComponent<AudioListener>().enabled = false;

        NetworkManager.Singleton.StartClient();
    }

    public void OnStartServer() {
        if (NetworkManager.Singleton.IsClient && NetworkManager.Singleton.IsServer) {
            return;
        }

        NetworkManager.Singleton.StartServer();
        NetworkManager.Singleton.SceneManager.LoadScene(sceneGameplay, LoadSceneMode.Single);
    }

    public void OnQuitClicked() {
        Application.Quit();
    }

    public void OnSettingsClicked() {

    }
}

using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuApplicationSettings : MonoBehaviour {
    [SerializeField] private TMP_Dropdown windowModeDropdown;
    [SerializeField] private TMP_Dropdown.OptionData wmFullscreenExclusive;
    [SerializeField] private TMP_Dropdown.OptionData wmFullscreenWindow;
    [SerializeField] private TMP_Dropdown.OptionData wmMaximizedWindow;
    [SerializeField] private TMP_Dropdown.OptionData wmWindowed;
    [SerializeField] private List<TMP_Dropdown.OptionData> windowModeOptions;

    private void Start() {
        windowModeDropdown.ClearOptions();

        wmFullscreenExclusive.text = "Fullscreen Exclusive";
        wmFullscreenWindow.text = "Fullscreen Window";
        wmMaximizedWindow.text = "Maximized Window";
        wmWindowed.text = "Windowed";

        windowModeOptions.Add(wmFullscreenExclusive);
        windowModeOptions.Add(wmFullscreenWindow);
        windowModeOptions.Add(wmMaximizedWindow);
        windowModeOptions.Add(wmWindowed);

        windowModeDropdown.AddOptions(windowModeOptions);
    }

    public void OnWindowModeChanged() {
        switch(windowModeDropdown.value) {
            case 0:
                Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
                break;
            case 1:
                Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
                break;
            case 2:
                Screen.fullScreenMode = FullScreenMode.MaximizedWindow;
                break;
            case 3:
                Screen.fullScreenMode = FullScreenMode.Windowed;
                break;
            default:
                break;
        }
    }
}
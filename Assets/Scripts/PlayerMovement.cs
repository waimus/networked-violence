using System.Collections;
using UnityEngine;
using Unity.Netcode;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : NetworkBehaviour {
    [Header("Movement")]
    [SerializeField] private float moveSpeed = 1.0f;
    [SerializeField] private GameObject pivotAimPoint;
    [SerializeField] private float gravityScale = 4.0f;
    [SerializeField] private float jumpHeight = 8.0f;
    [SerializeField] private Vector2 maxVelocity = new Vector2(24.0f, 32.0f);

    [Header("Spawning")]
    [SerializeField] private GameObject[] spawnPoints;

    private float horizontalVelocity;
    private float verticalVelocity;
    private Rigidbody2D physicsBody;

    public override void OnNetworkSpawn() {
        // Disable this script if this instance is not host.
        if (!IsOwner) {
            this.enabled = false;
        } else {
            // NetworkManager.Singleton.SceneManager.OnSceneEvent += OnNetworkSceneLoaded;
            StartCoroutine(TrySpawnTimed(0.7f));
        }
    }

    // private void OnNetworkSceneLoaded(SceneEvent sceneEvent) {
    //     switch(sceneEvent.SceneEventType) {
    //         case SceneEventType.LoadEventCompleted:
    //             break;
    //         default:
    //             break;
    //     }
    // }

    // Start is called before the first frame update
    void Start() {
        physicsBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        AimFollowCursor();

        HandleEscapeToMenu();

        // Horizontal input
        horizontalVelocity = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;

        // Vertical input: jumping
        if (Input.GetButtonDown("Jump")) {
			verticalVelocity = Mathf.Sqrt(jumpHeight * -2.0f * (Physics2D.gravity.y * gravityScale));
        }

        // Gravity
		verticalVelocity += Physics2D.gravity.y * gravityScale * Time.deltaTime;
		verticalVelocity = Mathf.Clamp(verticalVelocity, Physics2D.gravity.y, maxVelocity.y);
    }

    private void FixedUpdate() {
        // Velocity container
        Vector2 velocity = new Vector2(horizontalVelocity * 100.0f, verticalVelocity);

		// Increment gravity scale when falling and reduce horizontal velocity
		if (physicsBody.velocity.y < 0.0f) {
			velocity.y *= 3;
		} else {
			velocity.y = verticalVelocity;
		}

        // Set velocity
        physicsBody.velocity = new Vector2(
            Mathf.Clamp(velocity.x, -maxVelocity.x, maxVelocity.x),
            velocity.y
        );
    }

    private void AimFollowCursor() {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = 0;

        pivotAimPoint.transform.rotation = Quaternion.LookRotation(
            Vector3.forward, 
            Camera.main.ScreenToWorldPoint(mousePosition) - transform.position
        );
    }

    private void TrySpawn() {
        spawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
        int selector = Random.Range(0, spawnPoints.Length);
        transform.position = spawnPoints[selector].transform.position;
    }

    private IEnumerator TrySpawnTimed(float t) {
        yield return new WaitForSeconds(t);
        TrySpawn();
    }

#region
    // Temporary stuff to go back to menu during gameplay
    private int escapeCount = 0;

    private void HandleEscapeToMenu() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            escapeCount++;
        }

        switch(escapeCount) {
            case 0:
            case 1:
                break;
            case 2:
                escapeCount = 0;

                if (NetworkManager.Singleton) {
                    NetworkManager.Singleton.SceneManager.LoadScene("SCN_MainMenu", UnityEngine.SceneManagement.LoadSceneMode.Single);
                    NetworkManager.Singleton.Shutdown();
                } else {
                    UnityEngine.SceneManagement.SceneManager.LoadScene("SCN_MainMenu");
                }
                break;
            default:
                break;
        }
    }
#endregion
}

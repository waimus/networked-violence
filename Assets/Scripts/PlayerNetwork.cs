using UnityEngine;
using Unity.Netcode;

public class PlayerNetwork : NetworkBehaviour {
    [Header("Prefab Configuration")]
    [Tooltip("Pivot of the character's head to aim")]
    [SerializeField] private GameObject pivotAimPoint;

    [Header("Network Configuration")]
    private NetworkVariable<PlayerNetworkData> networkDataState = new(writePerm: NetworkVariableWritePermission.Owner);

    private Vector3 positionVelocity;
    private float rotationVelocity;
    [Tooltip("t value of lerp")]
    [SerializeField] private float interpolationTime = 0.1f;

    // Update is called once per frame
    void Update() {
        if (IsOwner) {
            // Save data when IsOwner
            networkDataState.Value = new PlayerNetworkData() {
                position = transform.position,
                rotation = pivotAimPoint.transform.rotation.eulerAngles
            };
        } else {
            // Apply data when not
            transform.position = Vector3.SmoothDamp(
                transform.position,
                networkDataState.Value.position,
                ref positionVelocity,
                interpolationTime
            );

            pivotAimPoint.transform.rotation = Quaternion.Euler(
                x: 0,
                y: 0,
                z: Mathf.SmoothDampAngle(
                    pivotAimPoint.transform.rotation.eulerAngles.z,
                    networkDataState.Value.rotation.z,
                    ref rotationVelocity,
                    interpolationTime
                )
            );
        }
    }

    // Implement custom data container to be sent over the network
    // to avoid sending unnecessary data
    struct PlayerNetworkData : INetworkSerializable {
        // Only need X and Y position in 2D side-scrolling game
        private float posX, posY;

        // And only need Z rotation
        private short rotZ;

        internal Vector3 position {
            get => new Vector3(posX, posY, 0);
            set {
                posX = value.x;
                posY = value.y;
            }
        }

        internal Vector3 rotation {
            get => new Vector3(0, 0, rotZ);
            set => rotZ = (short)value.z;
        }

        // Tell Unity how to serialize these data
        // Necessary: part of INetworkSerializable
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter {
            serializer.SerializeValue(ref posX);
            serializer.SerializeValue(ref posY);

            serializer.SerializeValue(ref rotZ);
        }
    }
}

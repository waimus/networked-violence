using UnityEngine;
using Unity.Netcode;

public class PlayerShooting : NetworkBehaviour {
    public GameObject projectilePrefab;
    [SerializeField] private Transform shootingPivot;

    public override void OnNetworkSpawn() {
        if (!IsOwner) {
            this.enabled = false;
        }
    }

    private void Update() {
        if (Input.GetButtonDown("Fire1")) {
            FireNewProjectileServerRpc();
        }
    }

    [ServerRpc]
    private void FireNewProjectileServerRpc() {
        GameObject projectile = NetworkObjectSpawner.SpawnNewNetworkObject(
            projectilePrefab,
            shootingPivot.position,
            shootingPivot.rotation
        );
        projectile.GetComponent<NetworkBehaviour>().NetworkObject.TryRemoveParent(true);
    }
}